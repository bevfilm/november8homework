﻿using System;

namespace AaronsHomeworkConsoleInput
{
    class Program
    {
        static void Main(string[] args)
        {
            int age;
            string height;
            int weight;

            Console.WriteLine("How old are you?: ");
            age = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("How tall are you?: ");
            height = Console.ReadLine();

            Console.WriteLine("How much do you weigh?: ");
            weight = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("So, you're {age} old, {height} tall and {weight} heavy. ");



        }
    }
}
