﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoopExercise
{
    class Program
    {
        static void Main(string[] args)

        {
            int num;
            

            Console.WriteLine("Write a number:");
            num = Convert.ToInt32(Console.ReadLine());

            

            for (int i = 0; i <= num; i++) {
                int cube = i* i * i;
                Console.WriteLine("The cube of " + i +" is " +cube);


            }
            Console.ReadLine();
        }
        
    }
}
