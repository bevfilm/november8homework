﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace loopshwnov28
{
    class Program
    {
        static void Main(string[] args)
        {
            // initialize var as i and make it equal to two
            int i = 2;
             
            // if this i variable is divided by 2 and there is no remainder, print that number then increment by 1.
            do
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine(i);
                }
                i++;
                

            }
            //continue to run this loop and print these numbers while i is less than 100.
            while (i < 100);

            Console.ReadLine();

            
        }
    }
}
