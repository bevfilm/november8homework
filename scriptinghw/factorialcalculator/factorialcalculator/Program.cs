﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace factorialcalculator
{
    class Program
    {
        static void Main()
        {
            int i, number, fact;

            Console.WriteLine("Enter your number:");
            number = Convert.ToInt32(Console.ReadLine());
            fact = number;
           
            for (i = number - 1; i >= 1; i--)
            {
                fact = fact * i;
            }
            Console.WriteLine("\nFactorial of Given Number is: " + fact);
            Console.ReadLine();


        }
    }
}
