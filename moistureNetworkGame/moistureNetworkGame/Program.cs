﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace moistureNetworkGame
{
    class Program
    {
        static void Main(string[] args)
        {
            int sex;
            int age;
            int answer;
            
            Console.WriteLine("Welcome to The Moisture Network, the premiere global social network for making connections.\nPlease sign-up");
            Console.ReadKey();
            Console.WriteLine("What is your name?");
            string name = Console.ReadLine();

            Console.WriteLine("Welcome {0}.", name);
            Console.ReadKey();

            Console.WriteLine("At Moisture Network, we bring together water seekers and friendship seekers.\n We just need to get some critical information from you first.\n Please press Enter");
            Console.ReadKey();

            Console.WriteLine("What is your sex?\n Press (1) for female and (2) for male.");
            sex = Convert.ToInt32(Console.ReadLine());

            if(sex == 1)
            {
                Console.WriteLine("Very nice to meet you! There are lots of opportunities for women on this platform.");
            }
            else
            {
                Console.WriteLine("There aren't a lot of people who trust men but there are still some great opportunities.");
            }
            Console.ReadLine();
            Console.WriteLine("How old are you?");
            age = Convert.ToInt32(Console.ReadLine());

            if (age > 40)
            {
                Console.WriteLine("Nice to have mature people as part of this community.");

            }
            else
            {
                Console.WriteLine("We welcome your energy!");
            }

            Console.WriteLine("You already have a message waiting for you?\n Press enter to see it.");
            Console.ReadKey();

            Console.WriteLine("There is a message from: Daniel Boom\n Will you accept this message?\n Press (1) for yes and (2) for No.");
            answer = Convert.ToInt32(Console.ReadLine());

            if (answer == 1)
            {
                Console.WriteLine("Message From David: 'Hey there. I see you're new. I always reach out to newbies.'");
            }
            else
            {
                Console.WriteLine("No worries. You will get more messages in the future.");
            }

            Console.ReadLine();
        }

      
    }
}
