﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classprojecthw120618
{
    class  MoistProfile2
    {
        private string name;
        private string emotionStatus;
        private int relateCurPts;
        private int relateBasePts;
        private int daysOnline;

        public void SetName(string Name)
        {
            this.name = Name; 
        }

        public string GetName()
        {
            return this.name; 
        }

        public void SetDaysOnline(int DaysOnline)
        {
            this.daysOnline = DaysOnline; 
        }
        public int GetDaysOnline()
        {
            return this.daysOnline; 
        }

        public void TimeOnline()
        {
            Console.WriteLine(name + " has spent " + daysOnline + " days on Moisture.");
        }       
    }
}
