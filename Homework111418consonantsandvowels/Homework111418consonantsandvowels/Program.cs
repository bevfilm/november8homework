﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework111418consonantsandvowels
{
    class Program
    {
        static void Main(string[] args)
        {
            string letter;
            string vowel1 = "a";
            string vowel2 = "e";
            string vowel3 = "i";
            string vowel4 = "o";
            string vowel5 = "u";

            Console.WriteLine("Enter a letter:");
           letter = Console.ReadLine();

            if (letter == vowel1 || letter == vowel2 || letter == vowel3 || letter == vowel4 || letter == vowel5 )
            {
                Console.WriteLine("Your letter is a vowel");
                Console.ReadLine();

            }

            else
            {
                Console.WriteLine("Your letter is a consonant.");
                Console.ReadLine();
            }


        }
    }
}
