﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fibonaccisequence
{
    class Program
    {
        static void Main(string[] args)
        {
            //initialize the integer variable where we will store user's number, the two starting numbers, output variable and i
            int number1, n1 = 0, n2 = 1, n3, i; 
            

            //user inputs their number
            Console.WriteLine("Enter your number:");
            number1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(n1 + " " + n2 + " "); 
            
            for (i = 2; i < number1; ++i)  
            {
                n3 = n1 + n2;
                Console.Write(n3 + " ");
                n1 = n2;
                n2 = n3;
            }

            Console.ReadLine();


        }
    }
}
