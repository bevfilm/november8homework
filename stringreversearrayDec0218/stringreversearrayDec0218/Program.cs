﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stringreversearrayDec0218
{
    class Program
    {
        static void Main(string[] args)
        {
            //store string in the var str.
            string str = "Cadillac";

            //convert the existing string "str" into an array. 
            char[] ch = str.ToCharArray();

            //use the Reverse method to reverse the order of the characters. 
            //Array.Reverse(ch);

            //Print the result to the console. 
            //Console.WriteLine(ch);

            //There is another way to do this using loops. I think I need to review loops again. 
            //https://www.dotnetperls.com/loop-string-array 

            for (int i = (ch.Length -1); i >= 0; i-- )
            {
                Console.WriteLine(ch[i]);//you need to remember how to access an array after you run a for loop. 
            }
            
            
            Console.ReadKey();

            
        }
    }
}
