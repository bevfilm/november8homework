﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arraysumproblem2Dec0218
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] totalScore = new int[5] { 5, 7, 3, 2, 9 };

            //I used the sum method to calculate the sum of the array.
            Console.WriteLine(totalScore.Sum());

            Console.ReadKey();
        }
    }
}
