﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeverlyHomeworkVariables110718
{
    class Program
    {
              


        static void Main(string[] args)
        {
        int cars = 100;
        float spaceInACar = 4.0f;
        float drivers = 30f;
        float passengers = 90f;
        float carsNotDriven = passengers- drivers;
        float carsDriven = drivers;
        float carPoolCapacity = spaceInACar * drivers;
        float averagePersonsPerCar = passengers / drivers; 


            Console.WriteLine($"There are {cars} cars available.");
            Console.WriteLine($"There are only {drivers} available.");
            Console.WriteLine($"There will be {carsNotDriven} empty cars today.");
            Console.WriteLine($"We can transport {carPoolCapacity} today.");
            Console.WriteLine($"We have {passengers} today.");
            Console.WriteLine($"We need to put {averagePersonsPerCar} in each car.");

            Console.ReadLine();

        }
    }
}
