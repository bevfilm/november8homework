﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gamehomeworkDec1118
{
    class Character
    {
        private string name;
        private string[] weapons = new string[5];
        private int life;


        public void SayHello()
        {
            Console.WriteLine("Your character name is:{0}", Name);
        }

        public void WeaponsList()
        {
            foreach (string value in Weapons)
            {
                Console.WriteLine(value);
            }
        }

        //1. create attack method
        //2. put villian life as parameter
        //3. generate random number to represent the "attack"
        //4. subtract the attack number from villian's life
        //5. Print the attack number and how much life is left in character
        //6. do same for villian and hero
        //7. whichever life is greater is the winner

        public void Attack()
        {
            Random random = new Random();
            int attackNum = random.Next(1, 101);


            int attackResults  = Life - attackNum;

            Console.WriteLine("{0} has {1} life left after attack.", Name, attackResults);

        }


        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string[] Weapons
        {
            get { return weapons; }
            set { weapons = value; }
        }

        public int Life
        {
            get { return life; }
            set { life = value; }
        }
    }

       

        






        class Hero : Character
        {

            public Hero()
            {
                Life = 100;


            }






        }

        class Villian : Character
        {
            public Villian()
            {
                Life = 100;
            }
        }
    }

