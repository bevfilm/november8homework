﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gamehomeworkDec1118
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //Instantiate an instance of the class Hero and Villian.
            Hero hero = new Hero();
            Villian villian = new Villian();
            


            //Ask user for her name
            Console.WriteLine("Enter your hero character name:");
            //store the hero name in the variable
            hero.Name = Console.ReadLine();

            Console.WriteLine("Enter your hero character weapons:\n Enter the the weapons separated by commas.\n Choose: knife, gun, sword, hammer, or axe ");
            //store the user's string into the hero weapons array. First convert string to an array.
            string heroWeapons = Console.ReadLine();
            hero.Weapons = heroWeapons.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

         
            Console.WriteLine("Enter the villian character name:");

            // store the villian name in the variable
            villian.Name = Console.ReadLine();

            Console.WriteLine("Enter your villian character weapons:\n Enter the the weapons separated by commas.\n Choose: knife, gun, sword, hammer, or axe ");
            //store the user's string into the hero weapons array. First convert string to an array.
            string villianWeapons = Console.ReadLine();
            villian.Weapons = villianWeapons.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);


            Console.WriteLine("\n=================================================================\n");
            Console.WriteLine("Your Hero Data.\n");
                                 
            //call the method that will print the hero name
            hero.SayHello();
            hero.WeaponsList();
            Console.ReadLine();

            Console.WriteLine("\n=================================================================\n");
            Console.WriteLine("Your Villian Data.\n");
            villian.SayHello();
            villian.WeaponsList();
            Console.ReadLine();

            Console.WriteLine("\n=================================================================\n");
            Console.WriteLine("Get Ready To Rumble!.\n");
            Console.WriteLine("\n=================================================================\n");

            Console.WriteLine("=====Results for Villian attack on Hero.=====\n");
            hero.Attack();
            Console.ReadLine();

            Console.WriteLine("=====Results for Hero attack on Villian.===== \n");
            villian.Attack();
            Console.ReadLine();

            if (hero.Life > villian.Life)
            {
                Console.WriteLine("{0} is the winner.", hero.Name);

            }
            else
            {
                Console.WriteLine("{0} is the winner.", villian.Name);
            }

            Console.ReadLine();



            //still trying to figure out how to use a loop to go through my array and select values according to
            //index number. I will figure this out in a separate solution so I can work through it. 

            //another method for getting user weapons is to create an array with weapons in your character class (done)
            //Then you need to provide the index number for each weapon to the user and ask them to indicate their 
            //weapon by the index number. Use a for each loop to run through the array and print the corresponding
            //weapon according to the index number.

            //I think the solution would look something like this: 

            //for (i = (heroWeapons.Length); i > 0; i++)
            //{
            //    Console.WriteLine(i);
            //}


        }
    }
}
