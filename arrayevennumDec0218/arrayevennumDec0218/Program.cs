﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arrayevennumDec0218
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] evenNum = new int[10] { 45, 25, 21, 56, 77, 81, 87, 91, 99, 100 };

            foreach (int result in evenNum)
            {
                if (result % 2 ==0)
                {
                    Console.WriteLine("{0} is an even number.", result);
                }
            }

            Console.ReadKey();
        }
    }
}
